"""o3mock requirements as tests"""
import subprocess


# Pass the cf conventions checker
def test_pass_cfchecker(netCDF_file):
    command = "cfchecks {}".format(netCDF_file)
    process = subprocess.run(command, shell=True)
    assert process.returncode == 0
