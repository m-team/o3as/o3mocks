# o3mocks
Mock utilities for o3as services

# Installation
From source:
```bash
pip install .
```
From requirements.txt file:
```txt
git+https://codebase.helmholtz.cloud/m-team/o3as/o3mocks@main
```

# Usage
Import from o3mocks the desired conventions and use a dataset generator.  
Here is an example to use in pytest:
```py
from o3mocks import cf_18

@fixture(scope="module", params=["toz_cf18.nc"])
def netCDF_file(request):
    cf_18.toz_dataset(filename=request.param)
    return request.param
```
